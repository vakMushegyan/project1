import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Question} from './interfaces';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  private url = 'assets/Data/Questions.json';
  constructor(private http: HttpClient) { }

  getQuestions(): Observable<Question[]> {
      return this.http.get<Question[]>(this.url);
  }

}
