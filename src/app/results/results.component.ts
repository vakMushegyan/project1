import { Component, OnInit } from '@angular/core';
import {QuestionService} from '../question.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  private Q = 'Q';
  private rightAnswers = 0;
  private allQuestions;
  private questionsArray = [];
  private answersArray = [];
  private answers = {};
  constructor(private questions: QuestionService, private router: ActivatedRoute, private route: Router) {
    this.answers = this.route.getCurrentNavigation().extras.state.obj;
    this.questions.getQuestions().subscribe( data => this.allQuestions = data.length );
    this.questions.getQuestions().subscribe( data => this.questionsArray = data );
  }
  ngOnInit() {
    console.log(this.questionsArray);
    for (const key in this.answers) {
      this.answersArray.push( {
        [key]: this.answers[key]
      } );
    }
    setTimeout( () => {
      for (let i = 0; i < this.answersArray.length; i++) {
        if (this.answersArray[i]['Q' + i] === this.questionsArray[i].correctAnswer ) {
          this.rightAnswers++;
        }
      }
    }, 1000 );
  }

  viewAnswers() {
    console.log(this.answers, this.rightAnswers);
    this.route.navigate(['viewAnswers'], {state: { obj: this.answers}});

  }
}
