import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {forbiddenPhoneNumber} from '../Validators/phone.validator';
import {validEmail} from '../Validators/email.validator';
import {Router} from '@angular/router';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent implements OnInit {
  userInput: FormGroup;
  constructor(private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.userInput = this.fb.group({
      phone: ['', [Validators.minLength(9), Validators.maxLength(9), forbiddenPhoneNumber]],
      email: ['', [validEmail]]
    });
  }

  goToQuestions() {
    // if (this.userInput.valid) {
    //   this.router.navigate(['questions']);
    // }
    this.router.navigate(['questions']);
  }

}
