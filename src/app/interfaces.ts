export interface Question {
  question: string;
  answers: [];
  correctAnswer: number;
}
