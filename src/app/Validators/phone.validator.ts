import {AbstractControl} from '@angular/forms';

export function forbiddenPhoneNumber(control: AbstractControl): {[key: string]: any } | null {
      const valid = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
      const forbidden = !valid.test(control.value);
      return forbidden ? {forbiddenPhone: {value: control.value}} : null;
}
