import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {QuestionService} from '../question.service';
import {Question} from '../interfaces';
import {Router} from '@angular/router';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {
  @ViewChild('radioForm', {static: false}) radioForm;
  private seconds = 0;
  private minutes = 0;
  constructor(private question: QuestionService, private fb: FormBuilder, private router: Router) { }
  questions: Question[];
  ngOnInit() {
    const time = setInterval( () => {
      ++this.seconds;

      if (this.seconds === 60) {
        this.seconds = 0;
        ++this.minutes;
      }
      if (this.minutes === 1) {
        clearInterval(time);
        this.log(this.radioForm);
      }
      }, 1000 );
    this.question.getQuestions().subscribe( data => this.questions = data );
  }

  log(radioForm) {
    this.router.navigate(['results'], {state: { obj: radioForm.value}});
  }

}
