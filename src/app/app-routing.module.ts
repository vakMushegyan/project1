import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {QuestionsComponent} from './questions/questions.component';
import {StartPageComponent} from './start-page/start-page.component';
import {ResultsComponent} from './results/results.component';
import {ViewAnswersComponent} from './view-answers/view-answers.component';


const routes: Routes = [
  {path: 'start', component: StartPageComponent},
  {path: '', redirectTo: '/start', pathMatch: 'full'},
  {path: 'questions', component: QuestionsComponent},
  {path: 'results', component: ResultsComponent},
  {path: 'viewAnswers', component: ViewAnswersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
