import { Component, OnInit } from '@angular/core';
import {QuestionService} from '../question.service';
import {Question} from '../interfaces';
import {Router} from '@angular/router';

@Component({
  selector: 'app-view-answers',
  templateUrl: './view-answers.component.html',
  styleUrls: ['./view-answers.component.css']
})
export class ViewAnswersComponent implements OnInit {
  constructor(private questions: QuestionService, private router: Router) {
    this.answers = this.router.getCurrentNavigation().extras.state.obj;
  }
  private answersArray = [];
  private answers = {};
  private question: Question[];
  ngOnInit() {
    this.questions.getQuestions().subscribe( data => this.question = data );
    for (const key in this.answers) {
      this.answersArray.push( {
        [key]: this.answers[key]
      } );
    }
  }

  log() {
    console.log(this.answersArray);
  }

}
